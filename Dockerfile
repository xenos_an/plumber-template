FROM registry.gitlab.b-data.ch/r/plumber:4.2.0

LABEL org.label-schema.license="MIT © 2020 b-data GmbH" \
      org.label-schema.vcs-url="https://gitlab.com/b-data/r/api/plumber-template" \
      maintainer="Olivier Benz <olivier.benz@b-data.ch>"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    zlib1g-dev \
  ## Clean up
  && rm -rf /tmp/* \
  && rm -rf /var/lib/apt/lists/*

COPY . .

RUN Rscript -e "renv::restore()"

## Configure container startup
ENTRYPOINT []
CMD ["Rscript", "main.R"]
